'''How to use Git
the first time setup
open up cygwin
cd /cygdrive/c/Users/PeiYing/Desktop/ethans-coding-things/Hangman
git init .
git add 'real hangman game.py' sprites/*
git remote add origin ssh://ethanfu168@challenge-bot.com:29418/ethanfu168/hangman.git

every time after that
open up cygwin

cd /cygdrive/c/Users/PeiYing/Desktop/ethans-coding-things/Hangman

git status
# find out which file has changes, press 'q' if stuck
git diff [filename]
# see the changes in a specific file, or all files if no file specified
git add file
git commit --message "new message"
git push

then edit the files again

# press 'q' if stuck
git diff
git add Ethan<TAB>
git commit --message "write a message in imperative voice"

to see all commits type <git log --oneline>
to get more info if dont type <--oneline>
'''
########################################################################
#Todo:
########################################################################
#Assign Underscores to a letter
#Make correct letters appear
#making an end screen
#make game end when hangman is dead
#make a used letters box
#make the face more blue for each body part added
#pick word out of a file
#make sprites transparent, then make screen black
########################################################################
#Todones:
########################################################################
#Then having the number of letters in lines
#making the stickman appear according to incorrect
#Then making it so that our text input sees if the letter is wrong or right
#have the text input saved
#making the stickman appear according to correct
#Have a randomly picked word
########################################################################
#Homeworks:#make the face more blue for each body part added
########################################################################
from kivy.app import App
import random
from kivy.uix.label import Label
from kivy.uix.widget import Widget
from kivy.graphics import Rectangle, Ellipse
from kivy.core.window import Window
from kivy.uix.textinput import TextInput
from kivy.clock import Clock
from kivy.core.window import Window
Window.clearcolor=(1,1,1,1)

words = ['secret', 'fart']
word = ''
word = random.choice(words)


class GameWidget(Widget):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)
        self._keyboard = Window.request_keyboard(self._on_keyboard_closed, self)
        self._keyboard.bind(on_key_down = self._on_key_down)
        self.keysPressed = set()
        for i in range(len(word)):
            self.underscore_appear(i)
        self.hanger_appear()


    def _on_keyboard_closed(self):
        self._keyboard.unbind(on_key_down = self._on_key_down)
        self._keyboard = None

    def _on_key_down(self, keyboard, keycode, text, modifiers):
        self.keysPressed.add(chr(keycode[0]))
        print("keycode =", str(keycode),
              "self.keysPressed = ", str(self.keysPressed))
        self.face_appear()
        self.body_appear()
        self.rArm_appear()
        self.lArm_appear()
        self.lLeg_appear()
        self.rLeg_appear()
        self.remaining_correct_letters(word, self.keysPressed)

    def remaining_correct_letters(self, word, letter_guesses):
        word = set(word)
        for letter in letter_guesses:
            word.discard(letter)
        if len(word) == 0:
            print ("GAME COMPLETE")
            
    def incorrect_letter_count(self, word, letter_guesses):
        wrong_letters = set()
        correct_letters = set()
        for letter in letter_guesses:
            if letter in word:
                correct_letters.add(letter)
            else:
                wrong_letters.add(letter)
        return len(wrong_letters)

    def hanger_appear(self):
        with self.canvas:
            Rectangle(source = "sprites/hanger.png",
                        pos=(100,300),
                        size = (200,200))
    def underscore_appear(self, which_one):
        with self.canvas:
            Rectangle(source = "sprites/underscore.png",
                      pos=(100 + 100 * which_one,100),
                      size = (50,50))
        #add label here
    def lbl_appear(self):
        lbl = Label(text = "Why this no work", color = [0,0,0])
        self.add_widget(lbl)


    def face_appear(self):
        if self.incorrect_letter_count(word,self.keysPressed)> 0:
            with self.canvas:
                Rectangle(source = "sprites/face.png",
                          pos=(220,420),
                          size = (75,75))
    def body_appear(self):
        if self.incorrect_letter_count(word,self.keysPressed)> 1:
            with self.canvas:
                Rectangle(source = "sprites/body.png",
                          pos=(210,335),
                          size= (100,100))
    def rArm_appear(self):
        if self.incorrect_letter_count(word,self.keysPressed)> 2:
            with self.canvas:
                Rectangle(source = "sprites/right arm.png",
                          pos=(260,370),
                          size=(50,50))
    def lArm_appear(self):
        if self.incorrect_letter_count(word,self.keysPressed)> 3:
            with self.canvas:
                Rectangle(source = "sprites/left arm.png",
                          pos=(210,370),
                          size=(50,50))
    def lLeg_appear(self):
        if self.incorrect_letter_count(word,self.keysPressed)> 4:
            with self.canvas:
                Rectangle(source = "sprites/right arm.png",
                          pos=(260,290),
                          size=(50,50))
    def rLeg_appear(self):
        if self.incorrect_letter_count(word,self.keysPressed)> 5:
            with self.canvas:
                Rectangle(source = "sprites/left arm.png",
                          pos=(210,290),
                          size=(50,50))
        
class MyApp(App):
    def build(self):
        return GameWidget()

if __name__ == "__main__":
    app = MyApp()
    app.run()
